# Home Assistant compatible mqtt sensor
# Home Assistant
# Released under the MIT licence.

from ha_mqtt_entity import HaMqttEntity


class HaMqttSensor(HaMqttEntity):

    def __init__(self, name):
        super().__init__(model=name, name="level")

        self.current_state['state'] = "OFF"

        self.discover_conf["state_topic"] = "{}/state".format(self.base_topic)
        self.discover_conf["command_topic"] = "{}/set".format(self.base_topic)
        self.discover_conf["value_template"] = '{{ value_json.state }}'
        self.discover_conf["device_class"] = "illuminance"
        self.discover_conf["icon"] = "mdi:LightbulbOn60"
        self.discover_conf["unit_of_measurement"] = "%"

        self.input_topics["{}/set".format(self.base_topic)] = self.set
        self.output_topics["{}/state".format(self.base_topic)] = self.state

    def set(self, payload):
        try:
            self.current_state['state'] = payload['state']
            self.is_updated = True
        except KeyError:
            pass

    def state(self):
        return self.current_state

