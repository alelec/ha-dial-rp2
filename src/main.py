import sys
sys.path.append("/")

import uasyncio as asyncio
from machine import Pin, PWM

from encoder_rp2 import Encoder
from settings import config

from abutton import Pushbutton

import ha_mqtt
from ha_mqtt_light import HaMqttBrightnessLight
from ha_mqtt_switch import HaMqttSwitch

# led = Pin(6, Pin.OUT)

enc_pin_a = Pin(2, Pin.IN, Pin.PULL_UP)
enc_pin_b = Pin(3, Pin.IN, Pin.PULL_UP)
encoder = Encoder(
    0,
    enc_pin_a,
    min=config["min"],
    max=config["max"],
    accel=config["accel"],
)


class LedEffects:
    def __init__(self, pin_num):
        self.pwm = PWM(Pin(pin_num, Pin.OUT))

        self._off = 0xFFFF - 1
        self._dim = 0xFFFF - 10
        self._medium = 0xFFFF - 100
        self._bright = 0xFFFF // 2

    def on(self):
        self.pwm.duty_u16(self._dim)

    def off(self):
        self.pwm.duty_u16(self._off)

    async def pulse(self, restore=True, length=30):
        current = self.pwm.duty_u16()

        self.pwm.duty_u16(self._off)
        await asyncio.sleep_ms(20)
        self.pwm.duty_u16(self._bright)
        await asyncio.sleep_ms(length)
        self.pwm.duty_u16(self._off)
        await asyncio.sleep_ms(20)

        if restore:
            self.pwm.duty_u16(current)
        return current

    async def pulse_long(self, restore=True):
        await self.pulse(restore=restore, length=120)

    async def pulse2(self):
        current = await self.pulse(restore=False)
        self.pwm.duty_u16(self._off)
        await asyncio.sleep_ms(80)
        await self.pulse(restore=False)
        self.pwm.duty_u16(current)



class BrightnessLightDriver:
    def __init__(self, pin_num, encoder):
        if pin_num is not None:
            self.pwm = PWM(Pin(pin_num, Pin.OUT))
        else:
            self.pwm = None
        self._encoder = encoder
        self.max = self._encoder.max
        self.prev = 9

    def brightness(self, value, set_enc=True):
        if self.pwm is not None:
            duty = 0xFFFF - int(0xFFFF * value / self.max)
            self.pwm.duty_u16(duty)
        if set_enc:
            self.prev = value
            self._encoder.value(value)

    def on(self):
        if self.pwm is not None:
            self.pwm.duty_u16(0)

    def off(self):
        if self.pwm is not None:
            self.pwm.duty_u16(0xFFFF)

    def __aiter__(self):  # See note below
        return self

    async def __anext__(self):
        self.prev = self._encoder.value()

        while True:
            await asyncio.sleep_ms(10)
            v = self._encoder.value()
            if v != self.prev:
                print(v)
                self.prev = v
                self.brightness(v, False)
                return v

led = LedEffects(6)
led.off()
ha_mqtt.led = led

light = BrightnessLightDriver(None, encoder)
sensor = HaMqttBrightnessLight(name=config["name"], light=light)

switch = Pushbutton(Pin(22, Pin.IN, Pin.PULL_UP), True)

hasw = HaMqttSwitch(name=config["name"])

async def button_press(*args):
    if hasw.toggle():
        await led.pulse2()
    else:
        await led.pulse()
    print("SW:", hasw.current_state['state'])

async def light_press(*args):
    if sensor.toggle():
        await led.pulse_long()
    else:
        await led.pulse()
    print("Light:", sensor.current_state['state'])

switch.long_func(button_press)
switch.release_func(light_press)

async def main():
    global light, sensor

    print("HA Dial rp2")
    async for v in light:
        sensor.set_brightness_state(v)


asyncio.run(main())
