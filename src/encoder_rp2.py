# encoder_rp2.py Uses the PIO for rapid response on RP2 chips (Pico)

# Copyright (c) 2022 Peter Hinch
# Released under the MIT License (MIT) - see LICENSE file

# PIO and SM code written by Sandor Attila Gerendi (@sanyi)
# https://github.com/micropython/micropython/pull/6894

from machine import Pin
from array import array
import rp2
import time

# Test with encoder on pins 2 and 3:
#e = Encoder(0, Pin(2))

#while True:
    #time.sleep(1)
    #print(e.value())

# Closure enables Viper to retain state. Currently (V1.17) nonlocal doesn't
# work: https://github.com/micropython/micropython/issues/8086
# so using arrays.
def make_isr(pos, _min: int, _max: int, _accel: int):
    old_x = array('i', (0,))
    # old_t is two values, the "previous time" and "moving average of interval"
    old_t = array('i', (time.ticks_ms(), 0))
    @micropython.viper
    def isr(sm):
        nonlocal old_x, old_t, _min, _max, _accel
        i = ptr32(pos)
        p = ptr32(old_x)
        to = ptr32(old_t)
        while sm.rx_fifo():
            # track time since last step
            t : int = int(time.ticks_ms())
            td : int = int(time.ticks_diff(t, to[0]))
            if td > 3: # Ignore bounce
                to[0] = t
                # moving average of td*1000
                to[1] -= to[1] // 4
                to[1]  += td * 250

                # accel
                a : int = int(_accel) // (to[1] // 1000)
                a : int = int(min(max(a, 1), 10))
                # print(f"    t: {td}, a: {a}")

                # encoder calcs
                v : int = int(sm.get()) & 3
                x : int = v & 1
                y : int = v >> 1
                s : int = 1 if (x ^ y) else -1
                n : int = i[0] + (s if (x ^ p[0]) else (0 - s)) * a
                if n >= int(_min) and n <= int(_max):
                    i[0] = n
                elif n < int(_min):
                    i[0] = n = int(_min)
                elif n > int(_max):
                    i[0] = n = int(_max)
                p[0] = x
    return isr

# Args:
# StateMachine no. (0-7): each instance must have a different sm_no.
# An initialised input Pin: this and the next pin are the encoder interface.
class Encoder:
    def __init__(self, sm_no, base_pin, scale=1, accel=25, min=0, max=100):
        self.scale = scale
        self.max = max
        self.min = min
        self._pos = array("i", (min,))  # [pos]
        try:
            rp2.PIO(0).remove_program()
        except:
            pass
        self.sm = rp2.StateMachine(sm_no, self.pio_quadrature, in_base=base_pin)
        self.sm.irq(make_isr(self._pos,  min, max, accel))  # Instantiate the closure
        self.sm.exec("set(y, 99)")  # Initialise y: guarantee different to the input
        self.sm.active(1)

    @rp2.asm_pio()
    def pio_quadrature(in_init=rp2.PIO.IN_LOW):
        wrap_target()
        label("again")
        in_(pins, 2)
        mov(x, isr)
        jmp(x_not_y, "push_data")
        mov(isr, null)
        jmp("again")
        label("push_data")
        push()
        irq(block, rel(0))
        mov(y, x)
        wrap()

    def position(self, value=None):
        if value is not None:
            self._pos[0] = round(value / self.scale)
        return self._pos[0] * self.scale

    def value(self, value=None):
        if value is not None:
            self._pos[0] = value
        return self._pos[0]
